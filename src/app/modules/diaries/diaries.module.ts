import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiariesComponent } from './diaries.component';
import { DiaryDetailComponent } from './components/diary-detail/diary-detail.component';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    DiariesComponent,
    DiaryDetailComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
  ],
  exports: [
    DiariesComponent,
    DiaryDetailComponent
  ]
})

export class DiariesModule { }
